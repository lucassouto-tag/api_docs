from django.shortcuts import get_object_or_404
from rest_framework.views import Response, status
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes, renderer_classes, action
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser
from rest_framework import permissions
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer
from rest_framework_swagger.views import SchemaGenerator
from .models import Livros
from .serializers import LivrosSerializers


@api_view()
@permission_classes((AllowAny, ))
@renderer_classes([OpenAPIRenderer, SwaggerUIRenderer])
def schema_view(request):

    generator = SchemaGenerator(title='Rest Swagger')

    return Response(generator.get_schema(request=request))

class IsNotDestroyOrIsAuthenticated(permissions.BasePermission):

    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            if view.action != 'destroy':
                return True
            return False
        return True

class TAGLivrosViewSet(viewsets.ViewSet):
    """
    Os manipuladores de método para um ViewSet
    são vinculados apenas às ações correspondentes
    no ponto de finalização da view, usando o método .as_view().

    Normalmente, em vez de registrar explicitamente as views 
    em um conjunto de views no urlconf,
    você registrará o viewset com uma classe de router,
    que determina automaticamente o urlconf para você.
    """

    def get_permissions(self):
        if self.action == 'destroy':
            self.permission_classes = [IsNotDestroyOrIsAuthenticated, ]
        return super(self.__class__, self).get_permissions()

    def list(self, request):
        queryset = Livros.objects.all()
        serializer = LivrosSerializers(queryset, many=True)

        return Response(data=serializer.data)

    def create(self, request):
        serializer = LivrosSerializers(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        queryset = Livros.objects.all()
        livro = get_object_or_404(queryset, pk=pk)
        serializer = LivrosSerializers(livro)

        return Response(serializer.data)

    def update(self, request, pk=None):
        queryset = Livros.objects.all()
        livro = get_object_or_404(queryset, pk=pk)
        serializer = LivrosSerializers(livro, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None):
        queryset = Livros.objects.all()
        livro = get_object_or_404(queryset, pk=pk)
        serializer = LivrosSerializers(livro, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        queryset = Livros.objects.all()
        livro = get_object_or_404(queryset, pk=pk)
        livro.delete()
        return Response(status=status.HTTP_202_ACCEPTED)
